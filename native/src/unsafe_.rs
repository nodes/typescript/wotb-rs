extern crate duniter_wotb;
extern crate neon;

use super::{build_distance_response, build_paths_response, DISTANCE_CALCULATOR, FILE_FORMATER,
            PATH_FINDER};
use duniter_wotb::data::rusty::RustyWebOfTrust;
use duniter_wotb::data::{HasLinkResult, NewLinkResult, NodeId, RemLinkResult, WebOfTrust};
use duniter_wotb::operations::distance::{DistanceCalculator, WotDistanceParameters};
use duniter_wotb::operations::file::FileFormater;
use duniter_wotb::operations::path::PathFinder;
use neon::js::{JsArray, JsBoolean, JsInteger, JsNumber, JsObject, JsString, Object};
use neon::vm::{Call, JsResult};
use std::collections::HashMap;
use std::ops::{Deref, DerefMut};

static mut WOT_INSTANCES: Option<HashMap<usize, Box<RustyWebOfTrust>>> = None;

pub fn get_wots() -> &'static mut HashMap<usize, Box<RustyWebOfTrust>> {
    unsafe {
        match WOT_INSTANCES {
            Some(ref mut x) => &mut *x,
            None => {
                WOT_INSTANCES = Some(HashMap::new());
                match WOT_INSTANCES {
                    Some(ref mut x) => &mut *x,
                    None => panic!(),
                }
            }
        }
    }
}

fn new_wot(max_links: usize) -> usize {
    let wots = get_wots();
    let mut instance_id = 0;
    while wots.contains_key(&instance_id) {
        instance_id += 1;
    }
    wots.insert(instance_id, Box::new(RustyWebOfTrust::new(max_links)));
    instance_id
}

fn get_wot<'a>(instance_id: usize) -> &'a RustyWebOfTrust {
    let wots = get_wots();
    let wot = wots.get(&instance_id);
    match wot {
        Some(wot) => wot.deref(),
        None => panic!("This instance don't exist !"),
    }
}

fn get_mut_wot<'a>(instance_id: usize) -> &'a mut RustyWebOfTrust {
    let wots = get_wots();
    let wot = wots.get_mut(&instance_id);
    match wot {
        Some(wot) => wot.deref_mut(),
        None => panic!("This instance don't exist !"),
    }
}

pub fn new_unsafe_file_instance(call: Call) -> JsResult<JsInteger> {
    let scope = call.scope;
    let file_path = try!(try!(call.arguments.require(scope, 0)).check::<JsString>()).value();
    let max_links = try!(try!(call.arguments.require(scope, 1)).check::<JsInteger>()).value();
    let (wot, _blockstamp): (RustyWebOfTrust, Vec<u8>) = FILE_FORMATER
        .from_file(&file_path, max_links as usize)
        .unwrap();
    let wots = get_wots();
    let mut instance_id = 0;
    while wots.contains_key(&instance_id) {
        instance_id += 1;
    }
    wots.insert(instance_id, Box::new(wot));
    Ok(JsInteger::new(scope, instance_id as i32))
}

pub fn write_unsafe_instance_in_file(call: Call) -> JsResult<JsBoolean> {
    let scope = call.scope;
    let instance_id =
        try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value() as usize;
    let file_path = try!(try!(call.arguments.require(scope, 1)).check::<JsString>()).value();
    let wot = get_wot(instance_id as usize);
    let header: Vec<u8> = Vec::with_capacity(0);
    match FILE_FORMATER.to_file(wot, &header, &file_path) {
        Ok(_) => Ok(JsBoolean::new(scope, true)),
        Err(e) => panic!("Fatal error : fail to write wot in file : {:?}", e),
    }
}

pub fn new_unsafe_memory_instance(call: Call) -> JsResult<JsInteger> {
    let scope = call.scope;
    let max_links = try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value();
    let _instance_id = new_wot(max_links as usize);
    Ok(JsInteger::new(scope, _instance_id as i32))
}

pub fn unsafe_mem_copy(call: Call) -> JsResult<JsInteger> {
    let scope = call.scope;
    let instance_id =
        try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value() as usize;
    let wots = get_wots();
    let wot_copy = wots.get(&instance_id)
        .expect("This instance don't exist !")
        .deref()
        .clone();
    let mut new_instance_id = 0;
    while wots.contains_key(&new_instance_id) {
        new_instance_id += 1;
    }
    wots.insert(new_instance_id, Box::new(wot_copy));
    Ok(JsInteger::new(scope, new_instance_id as i32))
}

pub fn unsafe_get_max_links(call: Call) -> JsResult<JsInteger> {
    let scope = call.scope;
    let instance_id = try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value();
    let mut _max_links = 0;
    _max_links = get_wot(instance_id as usize).get_max_link();
    Ok(JsInteger::new(scope, _max_links as i32))
}

pub fn unsafe_get_wot_size(call: Call) -> JsResult<JsInteger> {
    let scope = call.scope;
    let instance_id = try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value();
    let mut _size = 0;
    _size = get_wot(instance_id as usize).size();
    Ok(JsInteger::new(scope, _size as i32))
}

pub fn unsafe_is_enabled(call: Call) -> JsResult<JsBoolean> {
    let scope = call.scope;
    let instance_id = try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value();
    let node_id = try!(try!(call.arguments.require(scope, 1)).check::<JsInteger>()).value();
    if get_wot(instance_id as usize)
        .is_enabled(NodeId(node_id as usize))
        .unwrap()
    {
        Ok(JsBoolean::new(scope, true))
    } else {
        Ok(JsBoolean::new(scope, false))
    }
}

pub fn unsafe_get_enabled(call: Call) -> JsResult<JsArray> {
    let scope = call.scope;
    let instance_id = try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value();
    let enabled = get_mut_wot(instance_id as usize).get_enabled();
    let mut js_array = JsArray::new(scope, enabled.len() as u32);
    let mut index: u32 = 0;
    for node_id in enabled {
        let _bool = try!(JsArray::set(
            *js_array.deref_mut(),
            index,
            JsInteger::new(scope, node_id.0 as i32),
        ));
        index += 1;
    }
    Ok(js_array)
}

pub fn unsafe_get_disabled(call: Call) -> JsResult<JsArray> {
    let scope = call.scope;
    let instance_id = try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value();
    let disabled = get_mut_wot(instance_id as usize).get_disabled();
    let js_array = JsArray::new(scope, disabled.len() as u32);
    let mut index: u32 = 0;
    for node_id in disabled {
        let _bool = try!(JsArray::set(
            *js_array.deref(),
            index,
            JsInteger::new(scope, node_id.0 as i32),
        ));
        index += 1;
    }
    Ok(js_array)
}

pub fn unsafe_get_sentries(call: Call) -> JsResult<JsArray> {
    let scope = call.scope;
    let instance_id = try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value();
    let sentry_requirement =
        try!(try!(call.arguments.require(scope, 1)).check::<JsInteger>()).value();
    let sentries = get_mut_wot(instance_id as usize).get_sentries(sentry_requirement as usize);
    let js_array = JsArray::new(scope, sentries.len() as u32);
    let mut index: u32 = 0;
    for node_id in sentries {
        let _bool = try!(JsArray::set(
            *js_array.deref(),
            index,
            JsInteger::new(scope, node_id.0 as i32),
        ));
        index += 1;
    }
    Ok(js_array)
}

pub fn unsafe_get_non_sentries(call: Call) -> JsResult<JsArray> {
    let scope = call.scope;
    let instance_id = try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value();
    let sentry_requirement =
        try!(try!(call.arguments.require(scope, 1)).check::<JsInteger>()).value();
    let non_sentries =
        get_mut_wot(instance_id as usize).get_non_sentries(sentry_requirement as usize);
    let js_array = JsArray::new(scope, non_sentries.len() as u32);
    let mut index: u32 = 0;
    for node_id in non_sentries {
        let _bool = try!(JsArray::set(
            *js_array.deref(),
            index,
            JsInteger::new(scope, node_id.0 as i32),
        ));
        index += 1;
    }
    Ok(js_array)
}

pub fn unsafe_exist_link(call: Call) -> JsResult<JsBoolean> {
    let scope = call.scope;
    let instance_id = try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value();
    let source = try!(try!(call.arguments.require(scope, 1)).check::<JsInteger>()).value();
    let target = try!(try!(call.arguments.require(scope, 2)).check::<JsInteger>()).value();
    let result =
        get_wot(instance_id as usize).has_link(NodeId(source as usize), NodeId(target as usize));
    if let HasLinkResult::Link(has_link) = result {
        Ok(JsBoolean::new(scope, has_link))
    } else {
        panic!("Fatal Error : {:?}", result)
    }
}

pub fn unsafe_set_max_links(call: Call) -> JsResult<JsBoolean> {
    let scope = call.scope;
    let instance_id = try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value();
    let max_links = try!(try!(call.arguments.require(scope, 1)).check::<JsInteger>()).value();
    let _result = get_mut_wot(instance_id as usize).set_max_link(max_links as usize);
    Ok(JsBoolean::new(scope, true))
}

pub fn unsafe_add_node(call: Call) -> JsResult<JsInteger> {
    let scope = call.scope;
    let instance_id = try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value();
    let _node_id = get_mut_wot(instance_id as usize).add_node();
    Ok(JsInteger::new(scope, _node_id.0 as i32))
}

pub fn unsafe_rem_node(call: Call) -> JsResult<JsInteger> {
    let scope = call.scope;
    let instance_id = try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value();
    if let Some(node_id) = get_mut_wot(instance_id as usize).rem_node() {
        Ok(JsInteger::new(scope, node_id.0 as i32))
    } else {
        panic!("Fatal error : wotb : you try to remove a node to empty wot !")
    }
}

pub fn unsafe_set_enabled(call: Call) -> JsResult<JsBoolean> {
    let scope = call.scope;
    let instance_id = try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value();
    let is_enabled = try!(try!(call.arguments.require(scope, 1)).check::<JsBoolean>()).value();
    let node_id = try!(try!(call.arguments.require(scope, 2)).check::<JsInteger>()).value();
    if let Some(enabled) =
        get_mut_wot(instance_id as usize).set_enabled(NodeId(node_id as usize), is_enabled)
    {
        Ok(JsBoolean::new(scope, enabled))
    } else {
        panic!("Fatal error : wotb : node id don't exist !")
    }
}

pub fn unsafe_add_link(call: Call) -> JsResult<JsInteger> {
    let scope = call.scope;
    let instance_id = try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value();
    let source = try!(try!(call.arguments.require(scope, 1)).check::<JsInteger>()).value();
    let target = try!(try!(call.arguments.require(scope, 2)).check::<JsInteger>()).value();
    let result = get_mut_wot(instance_id as usize)
        .add_link(NodeId(source as usize), NodeId(target as usize));
    if let NewLinkResult::Ok(links_count) = result {
        Ok(JsInteger::new(scope, links_count as i32))
    } else {
        panic!("Fatal error wotb : {:?}", result)
    }
}

pub fn unsafe_rem_link(call: Call) -> JsResult<JsInteger> {
    let scope = call.scope;
    let instance_id = try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value();
    let source = try!(try!(call.arguments.require(scope, 1)).check::<JsInteger>()).value();
    let target = try!(try!(call.arguments.require(scope, 2)).check::<JsInteger>()).value();
    let result = get_mut_wot(instance_id as usize)
        .rem_link(NodeId(source as usize), NodeId(target as usize));
    if let RemLinkResult::Removed(links_count) = result {
        Ok(JsInteger::new(scope, links_count as i32))
    } else {
        panic!("Fatal Error : {:?}", result)
    }
}

pub fn unsafe_compute_distance(call: Call) -> JsResult<JsObject> {
    let scope = call.scope;
    let instance_id = try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value();
    let node_id = try!(try!(call.arguments.require(scope, 1)).check::<JsInteger>()).value();
    let sentry_requirement =
        try!(try!(call.arguments.require(scope, 2)).check::<JsInteger>()).value() as u32;
    let step_max = try!(try!(call.arguments.require(scope, 3)).check::<JsInteger>()).value() as u32;
    let x_percent = try!(try!(call.arguments.require(scope, 4)).check::<JsNumber>()).value() as f64;
    let distance_params = WotDistanceParameters {
        node: NodeId(node_id as usize),
        sentry_requirement,
        step_max,
        x_percent,
    };
    let wot = get_wot(instance_id as usize);
    build_distance_response(
        scope,
        &DISTANCE_CALCULATOR
            .compute_distance(wot, distance_params)
            .unwrap(),
    )
}

pub fn unsafe_find_paths(call: Call) -> JsResult<JsArray> {
    let scope = call.scope;
    let instance_id =
        try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value() as usize;
    let from = try!(try!(call.arguments.require(scope, 1)).check::<JsInteger>()).value() as u32;
    let to = try!(try!(call.arguments.require(scope, 2)).check::<JsInteger>()).value() as u32;
    let step_max = try!(try!(call.arguments.require(scope, 3)).check::<JsInteger>()).value() as u32;
    let wot = get_wot(instance_id as usize);
    Ok(try!(build_paths_response(
        scope,
        &PATH_FINDER.find_paths(wot, NodeId(from as usize), NodeId(to as usize), step_max)
    )))
    /*let mut i: u32 = 0;
    let js_paths = JsArray::new(scope, paths.len() as u32);
    for path in paths {
        let mut j: u32 = 0;
        let js_path = JsArray::new(scope, path.len() as u32);
        for node_id in path {
            let _bool = try!(JsArray::set(
                *js_path.deref(),
                j,
                JsInteger::new(scope, node_id.0 as i32),
            ));
            j += 1;
        }
        let _bool = try!(JsArray::set(*js_paths.deref(), i, js_path,));
        i += 1;
    }
    Ok(js_paths)*/
}

pub fn unsafe_remove_wot(call: Call) -> JsResult<JsBoolean> {
    let scope = call.scope;
    let instance_id =
        try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value() as usize;
    let wots = get_wots();
    if wots.len() > instance_id {
        wots.remove(&instance_id).unwrap();
        Ok(JsBoolean::new(scope, true))
    } else {
        Ok(JsBoolean::new(scope, false))
    }
}

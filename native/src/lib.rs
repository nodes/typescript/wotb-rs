#[macro_use]
extern crate neon;

extern crate duniter_wotb;

pub mod unsafe_;

use self::unsafe_::*;
use duniter_wotb::data::rusty::RustyWebOfTrust;
use duniter_wotb::data::{NodeId, WebOfTrust};
use duniter_wotb::operations::centrality::{CentralitiesCalculator,
                                           UlrikBrandesCentralityCalculator};
use duniter_wotb::operations::distance::{DistanceCalculator, RustyDistanceCalculator, WotDistance,
                                         WotDistanceParameters};
use duniter_wotb::operations::file::{BinaryFileFormater, FileFormater};
use duniter_wotb::operations::path::{PathFinder, RustyPathFinder};
use neon::js::{JsArray, JsBoolean, JsInteger, JsNumber, JsObject, JsString, JsValue, Object};
use neon::scope::Scope;
use neon::vm::{Call, JsResult};
use std::ops::{Deref, DerefMut};

static CENTRALITY_CALCULATOR: UlrikBrandesCentralityCalculator =
    UlrikBrandesCentralityCalculator {};
static DISTANCE_CALCULATOR: RustyDistanceCalculator = RustyDistanceCalculator {};
static FILE_FORMATER: BinaryFileFormater = BinaryFileFormater {};
static PATH_FINDER: RustyPathFinder = RustyPathFinder {};

fn safe_mem_copy(call: Call) -> JsResult<JsObject> {
    let scope = call.scope;
    let arg_nodes = try!(try!(try!(call.arguments.require(scope, 0)).check::<JsArray>()).to_vec(scope));
    let arg_issued_count = try!(try!(try!(call.arguments.require(scope, 1)).check::<JsArray>()).to_vec(scope));
    let arg_sources = try!(try!(try!(call.arguments.require(scope, 2)).check::<JsArray>()).to_vec(scope));
    let mut js_nodes_array = JsArray::new(scope, arg_nodes.len() as u32);
    let mut js_issued_count_array = JsArray::new(scope, arg_issued_count.len() as u32);
    let mut js_sources_array = JsArray::new(scope, arg_sources.len() as u32);
    let mut i: u32 = 0;
    for js_node in arg_nodes {
        let _bool = try!(JsArray::set(
                *js_nodes_array.deref_mut(),
                i,
                try!(js_node.check::<JsBoolean>()),
        ));
        i += 1;
    }
    let mut i: u32 = 0;
    for js_issued_count in arg_issued_count {
        let _bool = try!(JsArray::set(
                *js_issued_count_array.deref_mut(),
                i,
                try!(js_issued_count.check::<JsInteger>()),
        ));
        i += 1;
    }
    let mut i: u32 = 0;
    for js_source in arg_sources {
        let _bool = try!(JsArray::set(
                *js_sources_array.deref_mut(),
                i,
                try!(js_source.check::<JsArray>()),
        ));
        i += 1;
    }
    let mut js_wot = JsObject::new(scope);
    let _bool = try!(JsObject::set(*js_wot.deref_mut(), "nodes", js_nodes_array,));
    let _bool = try!(JsObject::set(*js_wot.deref_mut(), "issued_count", js_issued_count_array,));
    let _bool = try!(JsObject::set(*js_wot.deref_mut(), "sources", js_sources_array,));
    Ok(js_wot)

}

fn unsafe_mem_copy_from_safe(call: Call) -> JsResult<JsInteger> {
    let scope = call.scope;
    let max_links = try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value();
    let mut wot = RustyWebOfTrust::new(max_links as usize);
    let arg_nodes = call.arguments.require(scope, 1);
    let arg_sources = call.arguments.require(scope, 2);
    let _bool = try!(fill_rust_wot(scope, arg_nodes, arg_sources, &mut wot));
    let wots = unsafe_::get_wots();
    let mut new_instance_id = 0;
    while wots.contains_key(&new_instance_id) {
        new_instance_id += 1;
    }
    wots.insert(new_instance_id, Box::new(wot));
    Ok(JsInteger::new(scope, new_instance_id as i32))
}

fn new_safe_file_instance(call: Call) -> JsResult<JsObject> {
    let scope = call.scope;
    let file_path = try!(try!(call.arguments.require(scope, 0)).check::<JsString>()).value();
    let max_links = try!(try!(call.arguments.require(scope, 1)).check::<JsInteger>()).value();
    let (wot, _blockstamp): (RustyWebOfTrust, Vec<u8>) = FILE_FORMATER
        .from_file(&file_path, max_links as usize)
        .unwrap();
    let mut js_nodes_array = JsArray::new(scope, wot.size() as u32);
    let mut js_sources_array = JsArray::new(scope, wot.size() as u32);
    let mut rs_issued_counts = vec![0; wot.size()];
    for i in 0..wot.size() {
        let _bool = try!(JsArray::set(
            *js_nodes_array.deref_mut(),
            i as u32,
            JsBoolean::new(scope, wot.is_enabled(NodeId(i as usize)).unwrap()),
        ));
        let sources = wot.get_links_source(NodeId(i as usize)).unwrap();
        let mut js_sources = JsArray::new(scope, sources.len() as u32);
        let mut j: u32 = 0;
        for source in sources {
            rs_issued_counts[source.0] += 1;
            let _bool = try!(JsArray::set(
                *js_sources.deref_mut(),
                j,
                JsInteger::new(scope, source.0 as i32),
            ));
            j += 1;
        }
        let _bool = try!(JsArray::set(
            *js_sources_array.deref_mut(),
            i as u32,
            js_sources,
        ));
    }
    let mut js_issued_count_array = JsArray::new(scope, wot.size() as u32);
    let mut i: u32 = 0;
    for issued_count in rs_issued_counts {
        let _bool = try!(JsArray::set(
            *js_issued_count_array.deref_mut(),
            i,
            JsInteger::new(scope, issued_count as i32),
        ));
        i += 1;
    }
    let mut js_wot = JsObject::new(scope);
    let _bool = try!(JsObject::set(*js_wot.deref_mut(), "nodes", js_nodes_array,));
    let _bool = try!(JsObject::set(*js_wot.deref_mut(), "issued_count", js_issued_count_array,));
    let _bool = try!(JsObject::set(*js_wot.deref_mut(), "sources", js_sources_array,));
    Ok(js_wot)
}

fn new_safe_empty_instance(call: Call) -> JsResult<JsObject> {
    let scope = call.scope;
    let mut js_wot = JsObject::new(scope);
    let js_nodes_array = JsArray::new(scope, 0 as u32);
    let js_issued_count_array = JsArray::new(scope, 0 as u32);
    let js_sources_array = JsArray::new(scope, 0 as u32);
    let _bool = try!(JsObject::set(*js_wot.deref_mut(), "nodes", js_nodes_array,));
    let _bool = try!(JsObject::set(*js_wot.deref_mut(), "issued_count", js_issued_count_array,));
    let _bool = try!(JsObject::set(*js_wot.deref_mut(), "sources", js_sources_array,));
    Ok(js_wot)
}

fn fill_rust_wot<'a, 'b, S: Scope<'a>>(
    scope: &mut S,
    arg_nodes: JsResult<'b, JsValue>,
    arg_sources: JsResult<'b, JsValue>,
    wot: &mut RustyWebOfTrust,
) -> JsResult<'a, JsBoolean> {
    let js_nodes_array = try!(try!(arg_nodes).check::<JsArray>());
    let js_sources_array = try!(try!(arg_sources).check::<JsArray>());
    let js_nodes_array = try!(js_nodes_array.to_vec(scope));
    let mut i = 0;
    for js_node in js_nodes_array {
        wot.add_node();
        if !try!(js_node.check::<JsBoolean>()).value() {
            wot.set_enabled(NodeId(i), false);
        }
        i += 1;
    }
    let js_sources_array = try!(js_sources_array.to_vec(scope));
    let mut i = 0;
    for js_sources in js_sources_array {
        let js_sources = try!(try!(js_sources.check::<JsArray>()).to_vec(scope));
        for js_source in js_sources {
            wot.add_link(
                NodeId(try!(js_source.check::<JsInteger>()).value() as usize),
                NodeId(i),
            );
        }
        i += 1;
    }
    Ok(JsBoolean::new(scope, true))
}

fn write_safe_file_instance(call: Call) -> JsResult<JsBoolean> {
    let scope = call.scope;
    let file_path = try!(try!(call.arguments.require(scope, 0)).check::<JsString>()).value();
    let max_links = try!(try!(call.arguments.require(scope, 1)).check::<JsInteger>()).value();
    let mut wot = RustyWebOfTrust::new(max_links as usize);
    let arg_nodes = call.arguments.require(scope, 2);
    let arg_sources = call.arguments.require(scope, 3);
    let _bool = try!(fill_rust_wot(scope, arg_nodes, arg_sources, &mut wot,));

    let mut js_object = JsObject::new(scope);
    let header: Vec<u8> = Vec::with_capacity(0);
    match FILE_FORMATER.to_file(&wot, &header, &file_path) {
        Ok(_) => {
            try!(JsObject::set(
                *js_object.deref_mut(),
                "res",
                JsBoolean::new(scope, true),
            ));
        }
        Err(e) => {
            panic!("Fatal error : fail to write wot in file : {:?}", e);
        }
    };
    Ok(JsBoolean::new(scope, true))
}

fn safe_compute_distance(call: Call) -> JsResult<JsObject> {
    let scope = call.scope;
    let max_links = try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value();
    let arg_nodes = call.arguments.require(scope, 1);
    let arg_sources = call.arguments.require(scope, 2);
    let mut wot = RustyWebOfTrust::new(max_links as usize);
    let _bool = try!(fill_rust_wot(scope, arg_nodes, arg_sources, &mut wot));
    let node_id = try!(try!(call.arguments.require(scope, 3)).check::<JsInteger>()).value();
    let sentry_requirement =
        try!(try!(call.arguments.require(scope, 4)).check::<JsInteger>()).value() as u32;
    let step_max = try!(try!(call.arguments.require(scope, 5)).check::<JsInteger>()).value() as u32;
    let x_percent = try!(try!(call.arguments.require(scope, 6)).check::<JsNumber>()).value() as f64;
    let distance_params = WotDistanceParameters {
        node: NodeId(node_id as usize),
        sentry_requirement,
        step_max,
        x_percent,
    };
    Ok(try!(build_distance_response(
        scope,
        &DISTANCE_CALCULATOR
            .compute_distance(&wot, distance_params)
            .unwrap(),
    )))
}

fn safe_compute_distance_all_members(call: Call) -> JsResult<JsArray> {
    let scope = call.scope;
    let max_links = try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value();
    let arg_nodes = call.arguments.require(scope, 1);
    let arg_sources = call.arguments.require(scope, 2);
    let mut wot = RustyWebOfTrust::new(max_links as usize);
    let _bool = try!(fill_rust_wot(scope, arg_nodes, arg_sources, &mut wot));
    let sentry_requirement =
        try!(try!(call.arguments.require(scope, 3)).check::<JsInteger>()).value() as u32;
    let step_max = try!(try!(call.arguments.require(scope, 4)).check::<JsInteger>()).value() as u32;
    let x_percent = try!(try!(call.arguments.require(scope, 5)).check::<JsNumber>()).value() as f64;
    let enabled_nodes = wot.get_enabled();
    let mut js_array = JsArray::new(scope, enabled_nodes.len() as u32);
    let mut i: u32 = 0;
    for node in enabled_nodes {
        let distance_params = WotDistanceParameters {
            node,
            sentry_requirement,
            step_max,
            x_percent,
        };
        try!(JsArray::set(
            *js_array.deref_mut(),
            i,
            try!(build_distance_response(
                scope,
                &DISTANCE_CALCULATOR
                    .compute_distance(&wot, distance_params)
                    .unwrap(),
            ))
        ));
        i += 1;
    }
    Ok(js_array)
}

fn safe_find_paths(call: Call) -> JsResult<JsArray> {
    let scope = call.scope;
    let max_links = try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value();
    let arg_nodes = call.arguments.require(scope, 1);
    let arg_sources = call.arguments.require(scope, 2);
    let mut wot = RustyWebOfTrust::new(max_links as usize);
    let _bool = try!(fill_rust_wot(scope, arg_nodes, arg_sources, &mut wot));
    let from = try!(try!(call.arguments.require(scope, 3)).check::<JsInteger>()).value() as usize;
    let to = try!(try!(call.arguments.require(scope, 4)).check::<JsInteger>()).value() as usize;
    let step_max = try!(try!(call.arguments.require(scope, 5)).check::<JsInteger>()).value() as u32;
    Ok(try!(build_paths_response(
        scope,
        &PATH_FINDER.find_paths(&wot, NodeId(from), NodeId(to), step_max),
    )))
}

fn safe_compute_centralities(call: Call) -> JsResult<JsArray> {
    let scope = call.scope;
    let max_links = try!(try!(call.arguments.require(scope, 0)).check::<JsInteger>()).value();
    let arg_nodes = call.arguments.require(scope, 1);
    let arg_sources = call.arguments.require(scope, 2);
    let mut wot = RustyWebOfTrust::new(max_links as usize);
    let _bool = try!(fill_rust_wot(scope, arg_nodes, arg_sources, &mut wot));
    let _step_max =
        try!(try!(call.arguments.require(scope, 3)).check::<JsInteger>()).value() as u32;
    Ok(try!(build_centralities_response(
        scope,
        &CENTRALITY_CALCULATOR.stress_centralities(&wot),
    )))
}

fn build_distance_response<'a, S: Scope<'a>>(
    scope: &mut S,
    wot_distance: &WotDistance,
) -> JsResult<'a, JsObject> {
    let &WotDistance {
        sentries,
        success,
        success_at_border,
        reached,
        reached_at_border,
        outdistanced,
    } = wot_distance;
    let js_object = JsObject::new(scope);
    let _bool = try!(JsObject::set(
        *js_object.deref(),
        "sentries",
        JsInteger::new(scope, sentries as i32),
    ));
    let _bool = try!(JsObject::set(
        *js_object.deref(),
        "success",
        JsInteger::new(scope, success as i32),
    ));
    let _bool = try!(JsObject::set(
        *js_object.deref(),
        "success_at_border",
        JsInteger::new(scope, success_at_border as i32),
    ));
    let _bool = try!(JsObject::set(
        *js_object.deref(),
        "reached",
        JsInteger::new(scope, reached as i32),
    ));
    let _bool = try!(JsObject::set(
        *js_object.deref(),
        "reached_at_border",
        JsInteger::new(scope, reached_at_border as i32),
    ));
    let _bool = try!(JsObject::set(
        *js_object.deref(),
        "outdistanced",
        JsBoolean::new(scope, outdistanced),
    ));
    Ok(js_object)
}

fn build_paths_response<'a, S: Scope<'a>>(
    scope: &mut S,
    paths: &Vec<Vec<NodeId>>,
) -> JsResult<'a, JsArray> {
    let mut i: u32 = 0;
    let js_paths = JsArray::new(scope, paths.len() as u32);
    for path in paths {
        let mut j: u32 = 0;
        let js_path = JsArray::new(scope, path.len() as u32);
        for node_id in path {
            let _bool = try!(JsArray::set(
                *js_path.deref(),
                j,
                JsInteger::new(scope, node_id.0 as i32),
            ));
            j += 1;
        }
        let _bool = try!(JsArray::set(*js_paths.deref(), i, js_path,));
        i += 1;
    }
    Ok(js_paths)
}

fn build_centralities_response<'a, S: Scope<'a>>(
    scope: &mut S,
    centralities: &Vec<u64>,
) -> JsResult<'a, JsArray> {
    let mut i: u32 = 0;
    let js_centralities = JsArray::new(scope, centralities.len() as u32);
    for centrality in centralities {
        let _bool = try!(JsArray::set(
            *js_centralities.deref(),
            i,
            JsInteger::new(scope, *centrality as i32),
        ));
        i += 1;
    }
    Ok(js_centralities)
}

register_module!(m, {
    m.export("safe_mem_copy", safe_mem_copy)?;
    m.export("unsafe_mem_copy_from_safe", unsafe_mem_copy_from_safe)?;
    m.export("new_safe_file_instance", new_safe_file_instance)?;
    m.export("new_safe_empty_instance", new_safe_empty_instance)?;
    m.export("write_safe_file_instance", write_safe_file_instance)?;
    m.export("safe_compute_distance", safe_compute_distance)?;
    m.export(
        "safe_compute_distance_all_members",
        safe_compute_distance_all_members,
    )?;
    m.export("safe_find_paths", safe_find_paths)?;
    m.export("safe_compute_centralities", safe_compute_centralities)?;
    m.export("new_unsafe_file_instance", new_unsafe_file_instance)?;
    m.export(
        "write_unsafe_instance_in_file",
        write_unsafe_instance_in_file,
    )?;
    m.export("new_unsafe_memory_instance", new_unsafe_memory_instance)?;
    m.export("unsafe_mem_copy", unsafe_mem_copy)?;
    m.export("unsafe_get_max_links", unsafe_get_max_links)?;
    m.export("unsafe_get_wot_size", unsafe_get_wot_size)?;
    m.export("unsafe_is_enabled", unsafe_is_enabled)?;
    m.export("unsafe_get_enabled", unsafe_get_enabled)?;
    m.export("unsafe_get_disabled", unsafe_get_disabled)?;
    m.export("unsafe_get_sentries", unsafe_get_sentries)?;
    m.export("unsafe_get_non_sentries", unsafe_get_non_sentries)?;
    m.export("unsafe_exist_link", unsafe_exist_link)?;
    m.export("unsafe_set_max_links", unsafe_set_max_links)?;
    m.export("unsafe_add_node", unsafe_add_node)?;
    m.export("unsafe_rem_node", unsafe_rem_node)?;
    m.export("unsafe_set_enabled", unsafe_set_enabled)?;
    m.export("unsafe_add_link", unsafe_add_link)?;
    m.export("unsafe_rem_link", unsafe_rem_link)?;
    m.export("unsafe_compute_distance", unsafe_compute_distance)?;
    m.export("unsafe_find_paths", unsafe_find_paths)?;
    m.export("unsafe_remove_wot", unsafe_remove_wot)?;
    Ok(())
});

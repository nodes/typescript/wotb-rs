"use strict";

const addon = require('./../lib/index');
var assert = require('assert');
var fs = require('fs');
var path = require('path');
var should = require('should');


testsSafePerfs()

function testsSafePerfs() {
    function newInstance(launchSafeTests) {
        return () => {
            let wot = addon.newEmptySafeMemoryInstance(100);
            launchSafeTests(wot);
        }
    }

    describe("wotb-rs performances tests", () => {
        describe('Basic operations', newInstance((wot) => {

            it('should add 1_000_000 nodes', function() {
                for (let i = 0; i < 1000000; i++) {
                    should.equal(wot.addNode(), i);
                }
                should.equal(wot.getWoTSize(), 1000000);
            });

            it('should add 500_000 links', function() {
                for (let i = 0; i < 500000; i++) {
                    for (let j = 0; j < 1; j++) {
                        wot.addLink(i, Math.random*(999999))
                    }
                }
            });

            it('should make a real mem copy', function() {
                let wot2 = wot.memCopy();
                wot2.addNode();
                wot2.addLink(0, 1);
                should.equal(wot.getWoTSize(), 1000000);
                should.equal(wot.existsLink(0, 1), false);
            });

            it('should make a unsafe mem copy', function() {
                var unsafe_wot = wot.unsafeMemCopy();
            });

            it('should make a unsafe mem copy and copy them', function() {
                var unsafe_wot = wot.unsafeMemCopy();
                unsafe_wot.memCopy();
            });
        }));
    });
}
"use strict";

const path = require('path');
const binding_path = path.resolve(path.join(__dirname,'../native'));
const binding = require(binding_path);

module.exports = {

    newFileInstance: (filePath, sigStock) => {
        const instance = Object.create(UnsafeWoTB)
        if (process.platform == 'win32') {
            //let bufferedPath = new Buffer(filePath,'ascii');
            instance.init(binding.new_unsafe_file_instance(filePath, sigStock))
        } else {
            instance.init(binding.new_unsafe_file_instance(filePath, sigStock));
        }
        instance.setFilePath(filePath)
        return instance
    },

    newEmptyFileInstance: (filePath, sigStock) => {
        const instance = Object.create(UnsafeWoTB)
        const id = binding.new_unsafe_memory_instance(sigStock);
        instance.init(id)
        instance.setFilePath(filePath)
        return instance
    },

    newMemoryInstance: (sigStock) => {
        const instance = Object.create(UnsafeWoTB)
        const id = binding.new_unsafe_memory_instance(sigStock);
        instance.init(id)
        return instance
    },

    newSafeFileInstance: (filePath, sigStock) => {
        var instance = Object.create(SafeWoTB)
        instance.setFilePath(filePath)
        instance.setMaxCert(sigStock)
        let instance_content = binding.new_safe_file_instance(filePath, sigStock);
        instance.nodes = instance_content.nodes;
        instance.issued_count = instance_content.issued_count;
        instance.sources = instance_content.sources;
        return instance
    },

    newEmptySafeFileInstance: (filePath, sigStock) => {
        var instance = Object.create(SafeWoTB)
        instance.setFilePath(filePath)
        instance.setMaxCert(sigStock)
        let instance_content = binding.new_safe_empty_instance();
        instance.nodes = instance_content.nodes;
        instance.issued_count = instance_content.issued_count;
        instance.sources = instance_content.sources;
        return instance
    },

    newEmptySafeMemoryInstance: (sigStock) => {
        var instance = Object.create(SafeWoTB)
        instance.setFilePath("")
        instance.setMaxCert(sigStock)
        let instance_content = binding.new_safe_empty_instance();
        instance.nodes = instance_content.nodes;
        instance.issued_count = instance_content.issued_count;
        instance.sources = instance_content.sources;
        return instance
    }
};

const UnsafeWoTB = {

    instanceID: -1,
    filePath: "",
  
    init: function(instanceId){
        this.instanceID = instanceId
    },

    /**
     * Eventually has a file path if it is a file instance.
     * @param filePath
     */
    setFilePath: function(filePath) {
        this.filePath = filePath
    },

    write: function() {
        return binding.write_unsafe_instance_in_file(this.instanceID, this.filePath);
    },

    memCopy: function() {
        const copy = Object.create(UnsafeWoTB)
        copy.instanceID = binding.unsafe_mem_copy(this.instanceID);
        return copy;
    },

    getMaxCert: function() {
        return binding.unsafe_get_max_links(this.instanceID);
    },

    getWoTSize: function() {
        return binding.unsafe_get_wot_size(this.instanceID);
    },

    isEnabled: function(nodeId) {
        return binding.unsafe_is_enabled(this.instanceID, nodeId);
    },

    getEnabled: function() {
        return binding.unsafe_get_enabled(this.instanceID);
    },

    getDisabled: function() {
        return binding.unsafe_get_disabled(this.instanceID);
    },

    getSentries: function(sentry_requirement) {
        return binding.unsafe_get_sentries(this.instanceID, sentry_requirement);
    },

    getNonSentries: function(sentry_requirement) {
        return binding.unsafe_get_non_sentries(this.instanceID, sentry_requirement);
    },

    existsLink: function(source, target) {
        return binding.unsafe_exist_link(this.instanceID, source, target);
    },

    setMaxCert: function(maxCert) {
        return binding.unsafe_set_max_links(this.instanceID, maxCert);
    },

    addNode: function() {
        return binding.unsafe_add_node(this.instanceID);
    },

    removeNode: function() {
        return binding.unsafe_rem_node(this.instanceID);
    },

    setEnabled: function(is_enabled, nodeId) {
        return binding.unsafe_set_enabled(this.instanceID, is_enabled, nodeId);
    },

    addLink: function(source, target) {
        return binding.unsafe_add_link(this.instanceID, source, target);
    },

    removeLink: function(source, target) {
        return binding.unsafe_rem_link(this.instanceID, source, target);
    },

    isOutdistanced: function(node, sentry_requirement, step_max, x_percent) {
        var distance_result = binding.unsafe_compute_distance(this.instanceID, node, sentry_requirement, step_max, x_percent);
        return distance_result.outdistanced;
    },

    detailedDistance: function(node, sentry_requirement, step_max, x_percent) {
        var distance_result = binding.unsafe_compute_distance(this.instanceID, node, sentry_requirement, step_max, x_percent);
        return {
            nbReached: distance_result.reached,
            nbSuccess: distance_result.success,
            nbSentries: distance_result.sentries,
            isOutdistanced: distance_result.outdistanced
        };
    },

    detailedDistanceV2: function(node, sentry_requirement, step_max, x_percent) {
        return binding.unsafe_compute_distance(this.instanceID, node, sentry_requirement, step_max, x_percent);
    },

    getPaths: function(from, to, step_max) {
        return binding.unsafe_find_paths(this.instanceID, from, to, step_max);
    },

    clear: function() {
        return binding.unsafe_remove_wot(this.instanceID);
    },

    resetWoT: function() {
        var sigStock = binding.unsafe_get_max_links(this.instanceID);
        binding.unsafe_remove_wot(this.instanceID);
        this.instanceID = binding.new_unsafe_memory_instance(sigStock);
    }
}

const SafeWoTB = {
    filePath: "",
    maxCerts: 0,
    nodes: [],
    issued_count: [],
    sources: [],

    /**
     * Eventually has a file path if it is a file instance.
     * @param filePath
     */
    setFilePath: function(filePath) {
        this.filePath = filePath
    },

    write: function() {
        return binding.write_safe_file_instance(this.filePath, this.maxCerts, this.nodes, this.sources);
    },

    unsafeMemCopy: function() {
        const unsafe_copy = Object.create(UnsafeWoTB)
        unsafe_copy.instanceID = binding.unsafe_mem_copy_from_safe(this.maxCerts, this.nodes, this.sources);
        return unsafe_copy;
    },

    memCopy: function() {
        var instance = Object.create(SafeWoTB)
        instance.setFilePath("")
        instance.setMaxCert(this.maxCerts)
        let instance_content = binding.safe_mem_copy(this.nodes, this.issued_count, this.sources);
        instance.nodes = instance_content.nodes;
        instance.issued_count = instance_content.issued_count;
        instance.sources = instance_content.sources;
        return instance
    },

    getMaxCert: function() {
        return this.maxCerts;
    },

    getWoTSize: function() {
        return this.nodes.length;
    },

    isEnabled: function(nodeId) {
        return this.nodes[nodeId];
    },

    getEnabled: function() {
        let enabled = new Array();
        let i=0;
        this.nodes.forEach(function(node) {
            if (node) {
                enabled.push(i);
            }
            i++;
        });
        return enabled;
    },

    getDisabled: function() {
        let disabled = new Array();
        let i=0;
        this.nodes.forEach(function(node) {
            if (!node) {
                disabled.push(i);
            }
            i++;
        });
        return disabled;
    },

    getSentries: function(sentry_requirement) {
        let sentries = new Array();
        let issued_count = this.issued_count;
        let sources = this.sources;
        let i=0;
        this.nodes.forEach(function(enabled) {
            if (enabled && sources[i] != undefined && sources[i].length >= sentry_requirement && issued_count[i] >= sentry_requirement) {
                sentries.push(i);
            }
            i++;
        });
        return sentries;
    },

    getNonSentries: function(sentry_requirement) {
        let nonSentries = new Array();
        let issued_count = this.issued_count;
        let sources = this.sources;
        let i=0;
        this.nodes.forEach(function(enabled) {
            if (enabled && (sources[i] == undefined || sources[i].length < sentry_requirement || issued_count[i] < sentry_requirement)) {
                nonSentries.push(i);
            }
            i++;
        });
        return nonSentries;
    },

    existsLink: function(source, target) {
        return this.sources[target] != undefined && this.sources[target].indexOf(source) >= 0;
    },

    setMaxCert: function(maxCert) {
        this.maxCerts = maxCert;
    },

    addNode: function() {
        this.nodes.push(true);
        let node_id = this.nodes.length - 1;
        this.issued_count[node_id] = 0;
        return node_id
    },

    removeNode: function() {
        this.nodes.pop();
        return this.nodes.length - 1;
    },

    setEnabled: function(is_enabled, nodeId) {
        this.nodes[nodeId] = is_enabled;
        return is_enabled;
    },

    addLink: function(source, target) {
        if (this.sources[target] == undefined) {
            this.sources[target] = new Array();
        }
        this.sources[target].push(source);
        this.issued_count[source]++;
        return this.sources[target].length
    },

    removeLink: function(source, target) {
        if (this.sources[target] != undefined) {
            let index = this.sources[target].indexOf(source);
            if (index > -1) {
                this.sources[target].splice(index, 1);
            }
            this.issued_count[source]--;
        }
        return this.sources[target].length
    },

    isOutdistanced: function(node, sentry_requirement, step_max, x_percent) {
        var distance_result = binding.safe_compute_distance(this.maxCerts, this.nodes, this.sources, node, sentry_requirement, step_max, x_percent);
        return distance_result.outdistanced;
    },

    detailedDistance: function(node, sentry_requirement, step_max, x_percent) {
        var distance_result = binding.safe_compute_distance(this.maxCerts, this.nodes, this.sources, node, sentry_requirement, step_max, x_percent);
        return {
            nbReached: distance_result.reached,
            nbSuccess: distance_result.success,
            nbSentries: distance_result.sentries,
            isOutdistanced: distance_result.outdistanced
        };
    },

    detailedDistanceV2: function(node, sentry_requirement, step_max, x_percent) {
        return binding.safe_compute_distance(this.maxCerts, this.nodes, this.sources,  node, sentry_requirement, step_max, x_percent);
    },

    allDetailedDistanceV2: function(sentry_requirement, step_max, x_percent) {
        return binding.safe_compute_distance_all_members(this.maxCerts, this.nodes, this.sources, sentry_requirement, step_max, x_percent);
    },

    getPaths: function(from, to, step_max) {
        return binding.safe_find_paths(this.maxCerts, this.nodes, this.sources, from, to, step_max);
    },
}
